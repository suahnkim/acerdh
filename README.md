# acerdh: automatic contrast enhancement using reversible data hiding
Author : Rolf Lussi, ZHAW Institute of Embedded Systems <br>
Contributor : Suah Kim, <a href="https://iaslab.org">iaslab</a> <br>
Last Changes: 2020-12-29 <br>

To run an example: <br>
run "main.m" in matlab <br>

This is an implementation of paper (<a href="https://iaslab.org/paper/[Accepted_version]Automatic_Contrast_Enhancement_using_Reversible_Data_Hiding.pdf">Accepted</a>, <a href="https://ieeexplore.ieee.org/document/7368603">Official version</a>) with optimization of lowest peak selection when there are multiple options. 
This is vector optimized implementation. A code that is easier to understand will be uploaded in the future.

# Code Repositories
The same code can be found @ gitlab: https://gitlab.com/suahnkim/acerdh and gitee: https://gitee.com/suahnkim/acerdh
